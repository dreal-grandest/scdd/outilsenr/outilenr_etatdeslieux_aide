# OutilEnR_EtatdesLieux_aide

Ce dépôt est ouvert spécifiquement stocker la page d'aide appelé lorsque l'on clique sur l'onglet Aide dans l'outil Etat des lieux EnR.

## Mettre à jour la page d'aide

Pour mettre à jour la <b> page d'aide </b>, éditer le script aide.Rmd en respectant la syntaxe markdown.
Les changements sont pris en compte directement après commit.

## afficher un bandeau d'information

Pour ajouter un <b>bandeau d'information sur la page d'accueil</b>, éditer le script bandeau.Rmd en ajoutant du texte entre les balises :  
```
\<div class = "bandeau"> texte affiché sur le bandeau \</div>.
```
Les changements sont pris en compte directement après commit. Si le script est vide, le bandeau ne sera pas affiché.

